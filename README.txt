kACARS Readme
01/11/2013
Developed for: phpVMS
http://www.phpVMS.net
Copyright:    FS-Products
Developed by: Jeffrey Kobus
http://www.fs-products.net

*******************************************

Back end Installation

kACARS.php needs to be placed into the following directory
/core/modules/kACARS/

kACARS_Chat.php needs to be installed into the following directory
/core/modules/kACARS_Chat

kACARSChatData.class.php needs to be placed in the following directory
kACARSPmData.class.php should be placed in the following folder if the PM System was purchased
/core/common

A new table must be created to allow the chat to work.  The table needs to be named with the same table prefix as all other phpvms tables.
If the standard tableprefix was used (phpvms_) then the table needs to be named phpvms_acarschat.  If the table prefix is different than name accordingly.

The included sql (phpvms_acarschat.sql) will create a table named phpvms_acarschat.  If your table prefix is different either modify the sql or rename the table after running the sql.

Columns in the acarschat table
id         INT(11)    Auto Increment
pilotid    INT(11)
message    TEXT
time       TEXT
timestamp  TEXT

Aircraft images -
Aircraft images need to be in the format of ICAO.jpg and located in the following directory.  (The images must be approximately 120 X 80.)
/images/aircraft
Example - Saab 340 image    /images/aircraft/S340.jpg  (The ICAO code MUST match the ICAO code that is attached to the aircraft in the database


********************************************
OPTIONS

Position Report Option -
Also included is a route_map.tpl.  If you replace your standard route_map.tpl with this one then all flights that are logged with this application will display position reports.  The route_map.tpl has some tips inside to show you want can be done with the popup bubbles.  The template may also be placed inside of the admin template folder so that while approving PIREPs you can see the actual flight path of the pilot.

METAR - 
There are two METAR reporting windows. Each is hooked to an airport that is loaded into the flight information box.  Departure will display the current live METAR report for the departure airport.  The arrival will do the same for the arrival airport.

Chart -
There are two Chart displaying windows.  Each is hooked to an airport that is loaded into the flight information box.  When the departure chart is selected the application will first go to the /charts/ folder on the users system to check for the chart.  If not chart is found it will then attempt to retreive the chart from the VA site.  If no chart is found for the departure airport the application will then attempt to find a chart from an outside source.  If it still can not find a chart than a message of "No Chart Available" will be displayed"  The arrival chart will do the same with the arrival ICAO code.  To load charts onto your VA the charts must be placed in the /images/charts/ directory of your root folder.  These files must be named as the ICAO and be in png format.  example - PHNL chart /images/charts/PHNL.png  (The ICAO must be in CAP's)

AFK Check - 
At the start of each flight a target number is randomly generated.  This number will be the number of minutes before the AFK Check will be triggered.  A timer starts as soon as the aircraft reaches 10000 ft.  Once the aircraft has remained above 10000 ft for the target number of minutes the AFK Check is triggered.  A message informing the pilot that the AFK Check is activated is sent to the pilots FS screen.  This message is displayed just like an ATIS report.  (If a pilot has modified his/her FS message they may not receive the display.)  A message box also opens up for the pilot to acknowledge that he/she is in fact at the keyboard.  While the messagebox is open a separate timer begins to count the timer required to acknowledge the AFK Check.  After it has been acknowledge the process repeats.  Multiple checks may happen on long flights.  Once the flight has landed and the poilot stops the flight the number of AFK attempts and the number of minutes before acknowledgement are logged into the flight log.  

PM System - 
If the PM System was purchased then the phpvms_kacars_pmlog.sql must be run using phpAdmin.  This will create the necessary table for the PM system. 

Site Message - 
If the Site Message option was purchase then the phpvms_settings.sql file must be run using phpAdmin.  This will create a phpvms setting.  This setting is the site message that is broadcast to all kACARS uses.  This setting can be found in on the admin side of the phpvms site.  Look under the general settings.  If the setting is not there and the sql has been run then you may need to clear the phpvms cache.  This can be done under the Maintenance options.   

******************************************** 

Client Installation

Run the .msi file.  Follow the instructions to install kACARS.  A shortcut will be placed on the desktop.  Click the shortcut to run kACARS.


********************************************

Requirements -

Windows installer 3.1
Windows .NET framework 3.5
FSUIPC


********************************************

Settings

Run kACARS.  At the top you will see a drop down menu. "Info".  Inside of the drop down meu will be "Settings".  Click on this.  A new window will open displaying the current settings.  Change these and then click the SAVE button.  All new settings will be changed and updated upon the closing of this window.


********************************************

Settings Explained

1. URLbase - This is the base URL for the phpVMS system.  This would be the same address as your home page.
2. PilotID - This is the pilot id for the pilot using the client application.
3. Password - This is the password for the same pilopt as above.
4. Seach by Flight # - With this box checked you can enter a flightnumber and retrieve the information for that flight.  With it unchecked the "Get Bid Info" will get the next flight in your bids.
5. Chat Log In - With this box checked the pilot will be logged into the chat server automatically.


********************************************

Windows Vista & Windows 7
Client users running Windows Vista or Windows 7 will need to run this application using the "Run as Administrator" option.  This option must be set at the program file NOT at the shortcut.

