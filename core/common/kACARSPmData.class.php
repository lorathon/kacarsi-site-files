<?php

/**
 * kACARS Pm Version 1.0
 * By Jeffrey Kobus
 * www.fs-products.net
 *  
 */  



class kACARSPmData extends CodonModule {		
		
	public function getMessages($pilotid) {
		$sql = 'SELECT *
			FROM '.TABLE_PREFIX.'kacars_pmlog
			WHERE `to` = '.$pilotid.'
			AND `hide` = 0
			ORDER BY `id` ASC';										   
		
		$ret = DB::get_results($sql);
		echo DB::$error;
		return $ret;
	}
		
	public function sendMessage($to, $from, $title, $msg, $alert=0) {
		$to = intval($to);
		$from = intval($from);
		$title = DB::escape($title);
		$msg = DB::escape($msg);
		$alert = intval($alert);
						
		$sql = "INSERT INTO ".TABLE_PREFIX."kacars_pmlog (
			`to`, `from`, `title`, `msg`, `alert`, `timestamp`)
			VALUES (
			'$to', '$from', '$title', '$msg', '$alert', UTC_TIMESTAMP())";
		
		$res = DB::query($sql);
		echo DB::$error;
	}
		
		
	public function deleteMessage($id) {
		$sql = 'UPDATE `'.TABLE_PREFIX.'kacars_pmlog`
			SET `hide`=1
			WHERE `id`='.$id;
		DB::query($sql);
		echo DB::$error;
	}		
}