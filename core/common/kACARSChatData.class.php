<?php

/**
 * kACARS Chat Version 1.0
 * By Jeffrey Kobus
 * www.fs-products.net
 *  
 */  



class kACARSChatData extends CodonModule 
{		
		
		public static function signon ($pilotid, $message, $time)
		{		
			$date = $pilotid;
				
			$sql = "INSERT INTO ".TABLE_PREFIX."acarschat (
					`pilotid`, `message`, `time`, `timestamp`)
					VALUES (
					'$pilotid', '$message', '$time', UTC_TIMESTAMP())";			
		
			$res = DB::query($sql);			
		}
		
		public static function getLast($pilotid)
		{
			
			$sql = 'SELECT *
					FROM '.TABLE_PREFIX.'acarschat
					WHERE `pilotid` = '.$pilotid.'				
					ORDER BY `id` DESC
					LIMIT 1';										   
			
			return DB::get_row($sql);
		
		}
		
		public static function getMessage($id)
		{
			
			$sql = 'SELECT *
					FROM '.TABLE_PREFIX.'acarschat
					WHERE `id` >= '.$id.'				
					ORDER BY `id` ASC';										   
			
			return DB::get_results($sql);
		}
		
		public static function send ($pilotid, $message, $time)
		{		
			$message = DB::escape($message);
							
			$sql = "INSERT INTO ".TABLE_PREFIX."acarschat (
					`pilotid`, `message`, `time`, `timestamp`)
					VALUES (
					'$pilotid', '$message', '$time', UTC_TIMESTAMP())";
		
			$res = DB::query($sql);			
		}
		
		public static function getAllforDay($month, $day, $year)
		{			
			$params = array(
				'DAY(timestamp)'		=> $day,				
				'MONTH(timestamp)'		=> $month,
				'YEAR(timestamp)'		=> $year						
				);
        
			$sql = 'SELECT *					
				FROM '.TABLE_PREFIX.'acarschat';	
		
			$sql .= DB::build_where($params);
				
			$sql .= 'Order by id DESC';
			
			$results = DB::get_results($sql);
			return $results;
		}			
}