<?php

/**
 * kACARS Chat Version 1.0
 * By Jeffrey Kobus
 * www.fs-products.net
 *  
 */  


class kACARS_Chat extends CodonModule {

	public function SignOn() {
		
		$signon = $_POST['signon'];		
		$data = explode("/", $signon);
		
		if(!is_numeric($data[0])) {
			# see if they are a valid pilot:
			preg_match('/^([A-Za-z]*)(\d*)/', $data[0], $matches);
			$pilot_code = $matches[1];
			$pilotid = intval($matches[2]) - Config::Get('PILOTID_OFFSET');
		} else {
			$pilotid = $data[0];
		}				
		
		$pilotinfo = PilotData::getPilotData($pilotid);			
		$pilotrealcode = PilotData::getPilotCode($pilotinfo->code, $pilotinfo->pilotid);		
		$time = date("H:i");			
		$message = '['.$time.']'.$pilotrealcode.' '.$pilotinfo->firstname.' '.$pilotinfo->lastname.':  Connected!';
		
		$obj = 'kACARSChatData';
		
		
		$obj::signon($pilotid, $message, $time);
		$log = $obj::getLast($pilotid);		
		echo $log->id;	
	}
	
	public static function Message() {
		
		$id = $_POST['id'];
		$obj = 'kACARSChatData';
		
		$results = $obj::getMessage($id);
		
		if (count($results)>0) {
			foreach ($results as $row) {
				echo $row->message;
				echo "/";
			}
			echo "0/".$row->id;
		}		
	}
	
	public static function SendMessage() {
		
		$id = $_POST['pilotid'];
		$message = $_POST['message'];
		
		if(!is_numeric($id)) {
			# see if they are a valid pilot:
			preg_match('/^([A-Za-z]*)(\d*)/', $id, $matches);
			$pilot_code = $matches[1];
			$pilotid = intval($matches[2]) - Config::Get('PILOTID_OFFSET');
		} else {
			$pilotid = $id;
		}
		
		$pilotinfo = PilotData::getPilotData($pilotid);		
		$pilotrealcode = PilotData::getPilotCode($pilotinfo->code, $pilotinfo->pilotid);		
		$time = date("H:i");				
		$sql = 'SELECT short FROM '.TABLE_PREFIX.'ranks WHERE rank="'.$pilotinfo->rank.'"';		
		$text = DB::escape($message);		
		$message = '['.$time.']'.$pilotrealcode.' '.$pilotinfo->firstname.' '.$pilotinfo->lastname.':  '.$text;			
		$obj = 'kACARSChatData';
		$obj::send($pilotid, $message, $time);
	}	
	
	public static function AcarsOnline() {
		
		$results = ACARSData::GetACARSData();		
		if (count($results) > 0) {
			foreach($results as $row) {
				$pilotinfo = PilotData::getPilotData($row->pilotid);
				$c = (array) $row; // Convert the object to an array			
				$pilot = PilotData::GetPilotCode($pilotinfo->code, $pilotinfo->pilotid);
				$firstname = $pilot->firstname;
				$lastname = $pilot->lastname;
				$flightnum = $c['flightnum'];
				$depicao = $c['depicao'];
				$arricao = $c['arricao'];
				$time = $c['timeremaining'];
				$dist = $c['distremain'];
				$phase = $c['phasedetail'];

				$message = $pilot.' '.$row->pilotname.'/'.$flightnum.'/'.$depicao.'/'.$arricao.'/'.$time.'/'.$dist.'/'.$phase;
			
				echo $message;
				echo "|";
			}
		}
	}
}