<?php

/*
* Coded by:
* Jeffrey Kobus
* www.fs-products.net
*/


class kACARS extends CodonModule {
 
	public function index() {
		
		/* Screen shot configuration */
		$ftp_address 	= "";
		$ftp_user 		= "";
		$ftp_password	= "";
		$ftp_SSL		= false;
		
		
		
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){ 
			$postText = file_get_contents('php://input');			
			$rec_xml = $postText;					 
			$xml = simplexml_load_string($rec_xml);	
				
			if(!is_numeric($xml->verify->pilotID)) {
				# see if they are a valid pilot:
				preg_match('/^([A-Za-z]*)(\d*)/', $xml->verify->pilotID, $matches);
				$pilot_code = $matches[1];
				$pilotid = intval($matches[2]) - Config::Get('PILOTID_OFFSET');
			} else {
				$pilotid = $xml->verify->pilotID;
			}
			
			$pilotinfo = PilotData::getPilotData($pilotid);
			$pilotcode = PilotData::getPilotCode($pilotinfo->code, $pilotinfo->pilotid);	
				
			switch($xml->switch->data) {
				case verify:		
					$results = Auth::ProcessLogin($xml->verify->pilotID, $xml->verify->password);
					if ($results){
						$params 				= $pilotinfo;
						$params->loginStatus 	= 1;
						$params->pilotcode 		= PilotData::getPilotCode($pilotinfo->code, $pilotinfo->pilotid);
						$params->avatar 		= PilotData::getPilotAvatar($pilotinfo->pilotid);
						$params->rankimage 		= RanksData::getRankImage($pilotinfo->rank);
						$params->signature 		= fileurl(SIGNATURE_PATH.'/'.$params->pilotcode.'.png');
						
						/* Screenshot */
						$params->ftp_address 	= $ftp_address;
						$params->ftp_user 		= $ftp_user;
						$params->ftp_password 	= $ftp_password;
						$params->ftp_ssl 		= $ftp_SSL;
												
						unset($params->password);
						unset($params->salt);
					} else {
						$params = array('loginStatus' => '0');
					}
					self::sendXML($params);
					break;
					
				case getBid:
					// Get the latest bid for the pilot
					$biddata = self::getLatestBid($pilotid);
					$aircraftinfo = OperationsData::getAircraftByReg($biddata->registration);
					$dep = OperationsData::getAirportInfo($biddata->depicao);
					$arr = OperationsData::getAirportInfo($biddata->arricao);
											
					if(count($biddata) == 1) {		
						if($aircraftinfo->enabled == 1) {
							$params = array(
								'flightStatus' 	   	=> '1',
								'flightNumber'     	=> $biddata->code.$biddata->flightnum,
								'aircraftReg'      	=> $biddata->registration,
								'aircraftICAO'     	=> $aircraftinfo->icao,
								'aircraftFullName' 	=> $aircraftinfo->fullname,
								'flightLevel'      	=> $biddata->flightlevel,
								'flightDistance'	=> $biddata->distance,
								'aircraftMaxPax'   	=> $aircraftinfo->maxpax,
								'aircraftCargo'    	=> $aircraftinfo->maxcargo,
								'depICAO'          	=> $biddata->depicao,
								'arrICAO'          	=> $biddata->arricao,
								'route'            	=> $biddata->route,
								'depTime'          	=> $biddata->deptime,
								'arrTime'          	=> $biddata->arrtime,
								'flightTime'       	=> $biddata->flighttime,
								'flightType'       	=> $biddata->flighttype,
								'aircraftName'     	=> $aircraftinfo->name,
								'aircraftRange'    	=> $aircraftinfo->range,
								'aircraftWeight'   	=> $aircraftinfo->weight,
								'aircraftCruise'   	=> $aircraftinfo->cruise,
								'depLat'			=> $dep->lat,
								'depLng'			=> $dep->lng,
								'arrLat'			=> $arr->lat,
								'arrLng'			=> $arr->lng
								);					
						} else {	
							$params = array(
								'flightStatus' 	   => '3');		// Aircraft Out of Service.							
							}			
					} else	{
						$params = array(
							'flightStatus' 	   => '2');	// You have no bids!								
					}
					self::sendXML($params);
					break;
						
				case getFlight:
					// Get schedule based on flight number
					$flightNumber = $xml->pirep->flightNumber;												
					self::findFlight($flightNumber);
					break;
						
				case getFlightInfo:				
					$flightNumber = $xml->sch->flightNumber;						
					self::findFlight($flightNumber);
					break;
	
				case liveupdate:	
					$lat = str_replace(",", ".", $xml->liveupdate->latitude);
					$lon = str_replace(",", ".", $xml->liveupdate->longitude);					
			
					# Get the distance remaining
					$depapt = OperationsData::GetAirportInfo($xml->liveupdate->depICAO);
					$arrapt = OperationsData::GetAirportInfo($xml->liveupdate->arrICAO);
					$dist_remain =
						round(SchedulesData::distanceBetweenPoints($lat, $lon, $arrapt->lat, $arrapt->lng));
		
					# Estimate the time remaining
					if($xml->liveupdate->groundSpeed > 0) {
						$minutes = round($dist_remain / $xml->liveupdate->groundSpeed * 60);
						$time_remain = self::ConvertMinutes2Hours($minutes);
					} else {
						$time_remain = '00:00';
					}
			
					$fields = array(
						'pilotid'        =>$pilotid,
						'flightnum'      =>$xml->liveupdate->flightNumber,
						'pilotname'      =>'',
						'aircraft'       =>$xml->liveupdate->registration,
						'lat'            =>$lat,
						'lng'            =>$lon,
						'heading'        =>$xml->liveupdate->heading,
						'alt'            =>$xml->liveupdate->altitude,
						'gs'             =>$xml->liveupdate->groundSpeed,
						'depicao'        =>$xml->liveupdate->depICAO,
						'arricao'        =>$xml->liveupdate->arrICAO,
						'deptime'        =>$xml->liveupdate->depTime,
						'arrtime'        =>'',
						'route'          =>$xml->liveupdate->route,
						'distremain'     =>$dist_remain,
						'timeremaining'  =>$time_remain,
						'phasedetail'    =>$xml->liveupdate->status,
						'online'         =>$xml->liveupdate->online,
						'messagelog'     =>$xml->liveupdate->messagelog,
						'client'         =>$xml->liveupdate->source,
					);
			
					ACARSData::UpdateFlightData($pilotid, $fields);
							
					$params = array(
						'distRemain'  => $dist_remain,
						'timeRemain'  => $time_remain								
						);
					self::sendXML($params);
					break;
							
				case pirep:						
					$flightinfo = SchedulesData::getProperFlightNum($xml->pirep->flightNumber);
					$code = $flightinfo['code'];
					$flightnum = $flightinfo['flightnum'];	
			
					# Make sure airports exist:
					#  If not, add them.
					if(!OperationsData::GetAirportInfo($xml->pirep->depICAO)) {
						OperationsData::RetrieveAirportInfo($xml->pirep->depICAO);
					}
			
					if(!OperationsData::GetAirportInfo($xml->pirep->arrICAO)) {
						OperationsData::RetrieveAirportInfo($xml->pirep->arrICAO);
					}
			
					# Get aircraft information
					$reg = trim($xml->pirep->registration);
					$ac = OperationsData::GetAircraftByReg($reg);
			
					# Load info
					/* If no passengers set, then set it to the cargo */
					$load = $xml->pirep->pax;
					if(empty($load))
						$load = $xml->pirep->cargo;						
			
					/* Fuel conversion - kAcars only reports in lbs */
					$fuelused = $xml->pirep->fuelUsed;
					if(Config::Get('LiquidUnit') == '0') {
						# Convert to KGs, divide by density since d = mass * volume
						$fuelused = ($fuelused * .45359237) / .8075;
					}
					# Convert lbs to gallons
					elseif(Config::Get('LiquidUnit') == '1') {
						$fuelused = $fuelused / 6.84;
					}
					# Convert lbs to kgs
					elseif(Config::Get('LiquidUnit') == '2') {
						$fuelused = $fuelused * .45359237;
					}
							
					foreach($xml->report as $report) {
						$rawdata['points'][] = array(
								'name'          => (string)$report->name,
								'time'			=> (string)$report->time,
								'lat'           => (string)$report->lat,
								'lng'           => (string)$report->lng,
								'alt'           => (string)$report->alt,
								'head'          => (string)$report->head,
								'gs'            => (string)$report->gs,
								'phase'         => (string)$report->phase,
								'warning'       => (string)$report->warning,
								'warningdetail' => (string)$report->warningdetail
								);
					}
	
					$data = array(
						'pilotid'		=>$pilotid,
						'code'			=>$code,
						'flightnum'		=>$flightnum,
						'depicao'		=>$xml->pirep->depICAO,
						'arricao'		=>$xml->pirep->arrICAO,
						'aircraft'		=>$ac->id,
						'flighttime'	=>$xml->pirep->flightTime,
						'flighttype'	=>$xml->pirep->flightType,
						'submitdate'	=>'NOW()',
						'comment'		=>DB::escape($xml->pirep->comments),
						'fuelused'		=>$fuelused,
						'route'       	=>$xml->liveupdate->route,
						'source'		=>$xml->liveupdate->source,
						'load'			=>$load,
						'landingrate'	=>$xml->pirep->landing,
						'rawdata'		=>$rawdata,
						'log'			=>$xml->pirep->log
						);							
			
					$ret = ACARSData::FilePIREP($pilotid, $data);
					$last = PIREPData::getLastReports($pilotid, 1);
			
					if ($ret) {
						$params = array(
							'pirepStatus' 	   => '1',
							'pirepID'          => $last->pirepid
							);	// Pirep Filed!							
					} else {
						$params = array(
							'pirepStatus' 	   => '2'
							);	// Please Try Again!							
					}
					
					$send = self::sendXML($params);
					
					/* Update Custom Columns */
					$fields = array(
								'event'			=>$xml->pirep->event,
								'online'		=>$xml->pirep->online,
								'flighttype'	=>$xml->pirep->flightType
								);						
					
					PIREPData::updatePIREPFields($last->pirepid, $fields);					
					break;
					
				case aircraft:
					self::getAllAircraft();
					break;	
							
				case airport:
					self::getAllAirports();
					break;
							
				case aircraftinfo:
					$aircraftinfo = OperationsData::getAircraftByReg($xml->pirep->registration);
				
					$params = array(								
						'aircraftReg'      => $aircraftinfo->registration,
						'aircraftICAO'     => $aircraftinfo->icao,
						'aircraftFullName' => $aircraftinfo->fullname,								
						'aircraftMaxPax'   => $aircraftinfo->maxpax,
						'aircraftCargo'    => $aircraftinfo->maxcargo,								
						'aircraftName'     => $aircraftinfo->name,
						'aircraftRange'    => $aircraftinfo->range,
						'aircraftWeight'   => $aircraftinfo->weight,
						'aircraftCruise'   => $aircraftinfo->cruise
						);	
							
					self::sendXML($params);
					break;
							
				case schedules:
					self::getAllSchedules($pilotinfo->ranklevel, $xml->sch->depICAO);
					break;
							
				case schedules_AC:
					self::getAllSchedules_AC($pilotinfo->ranklevel, $xml->sch->aircraft);
					break;
						
				case bidFlight:
					
					$biddata = self::getLatestBid($pilotid);
					if($biddata && DISABLE_BIDS_ON_BID) { 
						$bidStatus = 2;
					} else {
						$ret = SchedulesData::AddBid($pilotid, $xml->sch->routeid);
						if($ret == true) 							
							$bidStatus = 1;
						else 								
							$bidStatus = 0;
					}
							
					$params = array('bidStatus' => $bidStatus);
					self::sendXML($params);
					break;
							
				case parseRoute:
					$dep = OperationsData::getAirportInfo($xml->liveupdate->depICAO);				
					self::getRoute($dep->lat, $dep->lng, $xml->liveupdate->route);
					break;
	
				case getPilots:
					self::getActivePilots();
					break;
					
				case getPMs:
					self::getPMs($pilotid);
					break;
					
				case sendPM:
					$to = $xml->pm->pm_toid;
					$from = $pilotid;
					$title = $xml->pm->pm_title;
					$msg = $xml->pm->pm_message;											
					kACARSPmData::sendMessage($to, $from, $title, $msg);
					break;
					
				case deletePM:
					kACARSPmData::deleteMessage($xml->pm->pm_id);
					break;
					
				case getMSG;
					$message = SettingsData::GetSettingValue('kACARS_MSG');
					$params = array('message' => $message);
					self::sendXML($params);
					break;
				
				case getNews:
					self::getNews();
					break;
				
				case getNewsItem:
					self::getNewsItem($xml->news->newsid);
					break;
				
				case getBids:
					self::getBids($pilotid);
					break;
				
				case deleteBid:
					SchedulesData::removeBid($xml->sch->bidid);
					break;
				
				case getLogbook:
					self::getLogbook($pilotid, $xml->logbook->startid);
					break;
				
				case getLogbookDetail:
					self::getLogbookDetail($xml->logbook->pirepid);
					break;
				
				case chatSendMessage:
					// Insert message and return this message id
					$text = DB::escape($xml->chat->chatmessage);
					$time = date("H:i");
					$message = '['.$time.']'.$pilotcode.' '.$pilotinfo->firstname.' '.$pilotinfo->lastname.':  '.$text;						
					$chatid = self::insertChatMessage($pilotid, $message, $time);
					self::sendXML(array('chatid' => $chatid));
					break;
				
				case chatGetMessage:
					// Check for any messages based on id sent
					self::getChatMessage(intval($xml->chat->chatid));					
					break;
				
				case onlinePilots:
					// Send online pilots list
					self::getOnlinePilots();
					break;
			}		
		}
	}
		
	private static function ConvertMinutes2Hours($Minutes) {
		if ($Minutes < 0) {
		    $Min = Abs($Minutes);
		} else {
		    $Min = $Minutes;
		}
		$iHours = Floor($Min / 60);
		$Minutes = ($Min - ($iHours * 60)) / 100;
		$tHours = $iHours + $Minutes;
		if ($Minutes < 0) {
		    $tHours = $tHours * (-1);
		}
		$aHours = explode(".", $tHours);
		$iHours = $aHours[0];
		if (empty($aHours[1])) {
		    $aHours[1] = "00";
		}
		$Minutes = $aHours[1];
		if (strlen($Minutes) < 2) {
		    $Minutes = $Minutes ."0";
		}
		$tHours = $iHours .":". $Minutes;
		return $tHours;
	}
	
	private static function getLatestBid($pilotid) {
		$pilotid = DB::escape($pilotid);
			
		$sql = 'SELECT s.*, b.bidid, a.id as aircraftid, a.name as aircraft, a.registration, a.maxpax, a.maxcargo
			FROM '.TABLE_PREFIX.'schedules s, 
			 '.TABLE_PREFIX.'bids b,
			 '.TABLE_PREFIX.'aircraft a
			WHERE b.routeid = s.id 
			AND s.aircraft=a.id
			AND b.pilotid='.$pilotid.'
			ORDER BY b.bidid ASC LIMIT 1';
			
		return DB::get_row($sql);
	}
		
	private static function sendXML($params) {
		$xml = new SimpleXMLElement("<sitedata />");
			
		$info_xml = $xml->addChild('info');
		foreach($params as $name => $value) {
			$info_xml->addChild($name, $value);
		}
			
		header('Content-type: text/xml'); 		
		$xml_string = $xml->asXML();
		echo $xml_string;
			
		# For debug
		#$this->log("Sending: \n".print_r($xml_string, true), 'kacars');
		return;	
	}
		
	private static function getAllAircraft() {
		$results = OperationsData::getAllAircraft(true);
		$xml = new SimpleXMLElement("<aircraftdata />");
		$info_xml = $xml->addChild('info');
		foreach($results as $row) {
			$info_xml->addChild('aircraftICAO', $row->icao);
			$info_xml->addChild('aircraftReg', $row->registration);
			$info_xml->addChild('aircraftName', $row->name);
			$info_xml->addChild('aircraftFullname', $row->fullname);
		}
		header('Content-type: text/xml'); 		
		echo $xml->asXML();
	}
		
	private static function getAllAirports() {
		$results = OperationsData::getAllAirports();
		$xml = new SimpleXMLElement("<airportdata />");
		$info_xml = $xml->addChild('info');
		foreach($results as $row) {
			$info_xml->addChild('airportICAO', $row->icao);
			$info_xml->addChild('airportName', $row->name);
			$info_xml->addChild('latitude', $row->lat);
			$info_xml->addChild('longitude', $row->lng);
		}
		header('Content-type: text/xml'); 		
		echo $xml->asXML();
	}
		
	private static function getAllSchedules($ranklevel, $icao) {
		$params['s.depicao'] = $icao;
		$params['s.enabled'] = 1;
		$results = SchedulesData::findSchedules($params);		
		$xml = new SimpleXMLElement("<scheduledata />");
		$info_xml = $xml->addChild('info');	
		if($results) {
			foreach($results as $row) {
				if ($row->aircraftlevel > $ranklevel) continue;
				
				// uncomment the following section to show only todays flights				
				/*
				$row->daysofweek = str_replace('7', '0', $row->daysofweek);	
				if(strpos($row->daysofweek, date('w')) === false)
					continue;
				*/
					
				if(DISABLE_SCHED_ON_BID && $row->bidid != 0) continue;
				
				$info = OperationsData::getAircraftByReg($row->registration);
				$info_xml->addChild('id', $row->id);
				$info_xml->addChild('flightnumber', $row->code.$row->flightnum);
				$info_xml->addChild('aircraft', $info->icao);
				$info_xml->addChild('depicao', $row->depicao);
				$info_xml->addChild('depname', $row->depname);			
				$info_xml->addChild('arricao', $row->arricao);
				$info_xml->addChild('arrname', $row->arrname);
				$info_xml->addChild('flighttime', $row->flighttime);
				$info_xml->addChild('deptime', $row->deptime);
				$info_xml->addChild('arrtime', $row->arrtime);
			}
		}			
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
	}
		
	private static function getAllSchedules_AC($ranklevel, $icao) {
		$params['a.icao'] = $icao;
		$params['s.enabled'] = 1;
		$results = SchedulesData::findSchedules($params);		
		$xml = new SimpleXMLElement("<scheduledata />");
		$info_xml = $xml->addChild('info');	
		$info_xml = $xml->addChild('test', $icao);
			
		if($results) {
			foreach($results as $row) {
				if ($row->aircraftlevel > $ranklevel) continue;
					
				// uncomment the following section to show only todays flights			
				/*
				$row->daysofweek = str_replace('7', '0', $row->daysofweek);	
				if(strpos($row->daysofweek, date('w')) === false)
					continue;
				*/
					
				$info = OperationsData::getAircraftByReg($row->registration);
				$info_xml->addChild('id', $row->id);
				$info_xml->addChild('flightnumber', $row->code.$row->flightnum);
				$info_xml->addChild('aircraft', $info->icao);
				$info_xml->addChild('depicao', $row->depicao);
				$info_xml->addChild('depname', $row->depname);			
				$info_xml->addChild('arricao', $row->arricao);
				$info_xml->addChild('arrname', $row->arrname);
				$info_xml->addChild('flighttime', $row->flighttime);
				$info_xml->addChild('deptime', $row->deptime);
				$info_xml->addChild('arrtime', $row->arrtime);
			}
		}			
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
	}
		
	private static function findFlight($flightNumber) {
		$flightinfo = SchedulesData::getProperFlightNum($flightNumber);
		$params['s.code'] = $flightinfo['code'];
		$params['s.flightnum'] = $flightinfo['flightnum'];
		$params['s.enabled'] = 1;
		$biddata = SchedulesData::findSchedules($params);
		$aircraftinfo = OperationsData::getAircraftByReg($biddata[0]->registration);
		
		if(count($biddata) == 1) {		
			$params = array(
				'flightStatus' 	   	=> '1',
				'flightNumber'     	=> $biddata[0]->code.$biddata[0]->flightnum,
				'aircraftReg'      	=> $biddata[0]->registration,
				'aircraftICAO'     	=> $aircraftinfo->icao,
				'aircraftFullName' 	=> $aircraftinfo->fullname,
				'flightLevel'      	=> $biddata[0]->flightlevel,
				'flightDistance'	=> $biddata[0]->distance,
				'aircraftMaxPax'   	=> $aircraftinfo->maxpax,
				'aircraftCargo'    	=> $aircraftinfo->maxcargo,
				'depICAO'          	=> $biddata[0]->depicao,
				'arrICAO'          	=> $biddata[0]->arricao,
				'route'            	=> $biddata[0]->route,
				'depTime'          	=> $biddata[0]->deptime,
				'arrTime'          	=> $biddata[0]->arrtime,
				'flightTime'       	=> $biddata[0]->flighttime,
				'flightType'       	=> $biddata[0]->flighttype,
				'aircraftName'     	=> $aircraftinfo->name,
				'aircraftRange'    	=> $aircraftinfo->range,
				'aircraftWeight'   	=> $aircraftinfo->weight,
				'aircraftCruise'   	=> $aircraftinfo->cruise,				
				'depLat'			=> $biddata[0]->deplat,
				'depLng'			=> $biddata[0]->deplng,
				'arrLat'			=> $biddata[0]->arrlat,
				'arrLng'			=> $biddata[0]->arrlng
				);								
		} else {	
			$params = array(
				'flightStatus' 	   => '2');								
		}
		$send = self::sendXML($params);
		return;
	}
		
	private static function getRoute($deplat, $deplng, $schedRoute) {
		$params->deplat = $deplat;
		$params->deplng = $deplng;
		$params->route = $schedRoute;
		$res = NavData::parseRoute($params);
		$xml = new SimpleXMLElement("<routedata />");
		$info_xml = $xml->addChild('info');
		
		if($res) {
			foreach($res as $row) {
				$info_xml->addChild('pointName', $row->name);
				$info_xml->addChild('pointLat', $row->lat);
				$info_xml->addChild('pointLng', $row->lng);
				$info_xml->addChild('type', $row->type);
			}
		}
		header('Content-type: text/xml'); 		
		echo $xml->asXML();
	}
		
	private static function getActivePilots() {
		$params= Array('retired'	=> '0');
		$results = PilotData::findPilots($params);	
		$xml = new SimpleXMLElement("<pilotsdata />");
		$info_xml = $xml->addChild('info');
		
		if($results) {
			foreach($results as $row) {
				$pilotcode = PilotData::getPilotCode($row->code, $row->pilotid);
				$info_xml->addChild('pilotID', $row->pilotid);
				$info_xml->addChild('pilotCode', $pilotcode);
				$info_xml->addChild('firstName', $row->firstname);
				$info_xml->addChild('lastName', $row->lastname);
			}			
		}
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
	}
		
	private static function getPMs($pilotid) {		
		$results = kACARSPmData::getMessages($pilotid);
		$xml = new SimpleXMLElement("<imdata />");
		$info_xml = $xml->addChild('info');
	
		if($results) {
			foreach($results as $row) {
				$sender = PilotData::getPilotData($row->from);
				$from = PilotData::getPilotcode($sender->code, $sender->pilotid).' '.$sender->firstname.' '.$sender->lastname;
				$message = str_replace("\n\n", "\r\n", $row->msg);
			
				$info_xml->addChild('msgid', $row->id);
				$info_xml->addChild('fromid', $row->from);
				$info_xml->addChild('from', $from);
				$info_xml->addChild('title', $row->title);
				$info_xml->addChild('message', $message);
				$info_xml->addChild('date', date(DATE_FORMAT, strtotime($row->timestamp)));
			}
		}	
		$info_xml->addChild('msg', count($results));		
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
	}
	
	private static function getNews() {
		$fsp_news = false;  // set to true if using the FS-Products FSP_News Module
		$count = '10'; // change to limit the number of news items sent to kACARS
		
		if(!$fsp_news) {
			$sql='SELECT
				id, subject, postedby, UNIX_TIMESTAMP(postdate) AS postdate
				FROM '.TABLE_PREFIX.'news 
				ORDER BY postdate DESC
				';
		} else {
			$sql='SELECT
				n.id, n.title, n.body, UNIX_TIMESTAMP(n.date_creation) AS postdate,
				CONCAT(p.firstname, " ", p.lastname) as postedby
				FROM '.TABLE_PREFIX.'fsp_news n
				LEFT JOIN '.TABLE_PREFIX.'pilots p ON p.pilotid = n.user_creation 
				ORDER BY postdate DESC
				';
		}
		if($count != '')
			$sql .= ' LIMIT '.$count;
		
		
		$results = DB::get_results($sql);
		
		$xml = new SimpleXMLElement("<imdata />");
		$info_xml = $xml->addChild('info');
	
		if($results) {
			foreach($results as $row) {
				$info_xml->addChild('newsid', $row->id);
				$info_xml->addChild('subject', $row->subject);
				$info_xml->addChild('postedby', $row->postedby);
				$info_xml->addChild('date', date(DATE_FORMAT, $row->postdate));
			}
		}		
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
	}
	
	public static function getNewsItem($newsid) {		
		$fsp_news = false;  // set to true if using the FS-Products FSP_News Module
		
		if(!$fsp_news) {
		$sql = 'SELECT body
			FROM '.TABLE_PREFIX.'news
			WHERE id = '.$newsid
			;
		
		} else {			
		
		$sql = 'SELECT body
			FROM '.TABLE_PREFIX.'fsp_news
			WHERE id = '.$newsid
			;
		}
			
		$result = DB::get_row($sql);		
		echo html_entity_decode($result->body);
	}
	
	private static function getBids($pilotid) {
		
		$results = SchedulesData::getBids($pilotid);
		
		$xml = new SimpleXMLElement("<scheduledata />");
		$info_xml = $xml->addChild('info');	
		if($results) {
			foreach($results as $row) {				
				$info = OperationsData::getAircraftByReg($row->registration);
				$dep = OperationsData::getAirportInfo($row->depicao);
				$arr = OperationsData::getAirportInfo($row->arricao);
				$info_xml->addChild('id', $row->bidid);
				$info_xml->addChild('flightnumber', $row->code.$row->flightnum);
				$info_xml->addChild('aircraft', $info->icao);
				$info_xml->addChild('depicao', $row->depicao);
				$info_xml->addChild('depname', $dep->name);			
				$info_xml->addChild('arricao', $row->arricao);
				$info_xml->addChild('arrname', $arr->name);
				$info_xml->addChild('flighttime', $row->flighttime);
				$info_xml->addChild('deptime', $row->deptime);
				$info_xml->addChild('arrtime', $row->arrtime);
			}
		}			
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
	}
	
	private static function getLogbook($pilotid, $start = 0) {
		
		if(trim($pilotid) == '') return;
		
		if(trim($start) == '') $start = 0;
		
		$sql = 'SELECT
				p.`pirepid`, p.`code`, p.`flightnum`, p.`accepted`,
				p.`depicao`, p.`arricao`, p.`flighttime_stamp`, p.`landingrate`,
				p.`submitdate`, p.`fuelused`, a.`icao` as aircrafticao,
				dep.`name` as depname, arr.`name` as arrname
			FROM '.TABLE_PREFIX.'pireps p
			LEFT JOIN '.TABLE_PREFIX.'aircraft a ON a.id = p.`aircraft`
			LEFT JOIN '.TABLE_PREFIX.'airports dep ON dep.`icao` = p.`depicao`
			LEFT JOIN '.TABLE_PREFIX.'airports arr ON arr.`icao` = p.`arricao`
			WHERE p.`pilotid` = '.intval($pilotid).'
			AND p.`pirepid` > '.$start.'
			ORDER BY p.`pirepid` DESC
			';
			
		$results = DB::get_results($sql);
		
		echo DB::$error;
		
		$xml = new SimpleXMLElement("<logbook />");
		$info_xml = $xml->addChild('info');
		
		$fuelunit = Config::Get('LiquidUnit');
		
		if($results) {
			foreach($results as $row) {	
				$info_xml->addChild('pirepid', $row->pirepid);
				$info_xml->addChild('flightnumber', $row->code.$row->flightnum);
				$info_xml->addChild('aircraft', $row->aircrafticao);
				$info_xml->addChild('depicao', $row->depicao);
				$info_xml->addChild('depname', $row->depname);
				$info_xml->addChild('arricao', $row->arricao);
				$info_xml->addChild('arrname', $row->arrname);
				$info_xml->addChild('flighttime', $row->flighttime_stamp);
				$info_xml->addChild('landingrate', $row->landingrate);
				$info_xml->addChild('fuelused', $row->fuelused);
				$info_xml->addChild('submitdate', date(DATE_FORMAT, strtotime($row->submitdate)));
				$info_xml->addChild('date', $row->submitdate);
				$info_xml->addChild('status', $row->accepted);
				$info_xml->addChild('fuelunit', $fuelunit);
			}
		}			
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
	}
	
	private static function getLogbookDetail($pirepid) {
		
		$sql = 'SELECT p.*, ac.*,
			dep.name as depname, dep.lat as deplat, dep.lng as deplng,
			arr.name as arrname, arr.lat as arrlat, arr.lng as arrlng
			FROM '.TABLE_PREFIX.'pireps p
			LEFT JOIN '.TABLE_PREFIX.'airports dep ON dep.icao = p.depicao
			LEFT JOIN '.TABLE_PREFIX.'airports arr ON arr.icao = p.arricao
			LEFT JOIN '.TABLE_PREFIX.'aircraft ac ON ac.id = p.aircraft
			WHERE p.pirepid = '.$pirepid
			;
			
		$results = DB::get_row($sql);
		$results->fuelunit = Config::Get('LiquidUnit');
		$results->date = $results->submitdate;
		$results->submitdate = date(DATE_FORMAT, strtotime($results->submitdate));		
		$results->price = FinanceData::FormatMoney($results->price);
		$results->fuelprice = FinanceData::FormatMoney($results->fuelprice);
		$results->fuelunitcost = FinanceData::FormatMoney($results->fuelunitcost);
		$results->pilotpay = FinanceData::FormatMoney($results->pilotpay);
		$results->gross = FinanceData::FormatMoney($results->gross);
		$results->expenses = FinanceData::FormatMoney($results->expenses);
		$results->revenue = FinanceData::FormatMoney($results->revenue);
		self::sendXML($results);		
	}
	
	private static function insertChatMessage ($pilotid, $message, $time) {
		
		$message = DB::escape($message);
							
		$sql = "INSERT INTO ".TABLE_PREFIX."acarschat (
				`pilotid`, `message`, `time`, `timestamp`)
				VALUES (
				'$pilotid', '$message', '$time', UTC_TIMESTAMP())";
		
		DB::query($sql);
		return DB::$insert_id;
	}
	
	private static function getChatMessage($id) {
			
		$sql = 'SELECT *
				FROM '.TABLE_PREFIX.'acarschat
				WHERE `id` >= '.$id.'				
				ORDER BY `id` ASC';										   
			
		$results = DB::get_results($sql);
	
		$xml = new SimpleXMLElement("<chat />");
		$info_xml = $xml->addChild('info');	
		if($results) {
			foreach($results as $row) {				
				$info_xml->addChild('id', $row->id);
				$info_xml->addChild('pilotid', $row->pilotid);
				$info_xml->addChild('message', $row->message);
				$info_xml->addChild('time', $row->time);
				$info_xml->addChild('timestamp', $row->timestamp);	
			}
		}			
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
	}
	
	private static function getOnlinePilots() {
		$results = ACARSData::GetACARSData();
		
		$xml = new SimpleXMLElement("<onlinepilots />");
		$info_xml = $xml->addChild('info');	
		if($results) {
			foreach($results as $row) {
				
				$pilotinfo = PilotData::getPilotData($row->pilotid);
				
				
				$info_xml->addChild('id', $row->id);
				$info_xml->addChild('pilotid', $row->pilotid);
				$info_xml->addChild('flightnum', $row->flightnum);
				$info_xml->addChild('pilotname', $row->pilotname);
				$info_xml->addChild('aircraft', $row->aircraft);
				$info_xml->addChild('lat', $row->lat);
				$info_xml->addChild('lng', $row->lng);
				$info_xml->addChild('heading', $row->heading);
				$info_xml->addChild('alt', $row->alt);
				$info_xml->addChild('gs', $row->gs);
				$info_xml->addChild('depicao', $row->depicao);
				$info_xml->addChild('depapt', $row->depapt);
				$info_xml->addChild('arricao', $row->arricao);
				$info_xml->addChild('arrapt', $row->arrapt);
				$info_xml->addChild('deptime', $row->deptime);
				$info_xml->addChild('timeremaining', $row->timeremaining);
				$info_xml->addChild('arrtime', $row->arrtime);
				$info_xml->addChild('route', $row->route);
				$info_xml->addChild('route_details', $row->route_details);
				$info_xml->addChild('distremain', $row->distremain);
				$info_xml->addChild('phasedetail', $row->phasedetail);
				$info_xml->addChild('online', $row->online);
				$info_xml->addChild('messagelog', $row->messagelog);
				$info_xml->addChild('lastupdate', $row->lastupdate);
				$info_xml->addChild('client', $row->client);
				$info_xml->addChild('pilotcode', PilotData::GetPilotCode($pilotinfo->code, $pilotinfo->pilotid));
			}
		}			
		header('Content-type: text/xml'); 		
		echo $xml->asXML();	
		
	}

}