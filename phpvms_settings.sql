INSERT INTO `phpvms_settings` 
(
`id`, 
`friendlyname`, 
`name`, 
`value`, 
`descrip`, 
`core`
) 
VALUES 
(
NULL, 
'kACARS Message', 
'kACARS_MSG', 
'Welcome!', 
'This will broadcast a message to all kACARS users.  This message is updated every 10 minutes inside of kACARS.', 
'1'
);