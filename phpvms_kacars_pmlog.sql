CREATE TABLE phpvms_kacars_pmlog
(
`id` int(11) AUTO_INCREMENT,
`from` int(11),
`to` int(11),
`title` text,
`msg` text,
`hide` int(1) NOT NULL DEFAULT '0',
`alert` int(1) NOT NULL DEFAULT '0',
`timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (id)
)